<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Big Days Celebration</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bootstrap 4 Mobile App Template">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700|Nunito:400,600,700" rel="stylesheet">
    
    <!-- FontAwesome JS-->
	<script defer src="landing-page/fontawesome/js/all.min.js"></script>
    
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="landing-page/plugins/jquery-flipster/dist/jquery.flipster.min.css">
    
    
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="landing-page/css/theme.css">

</head> 

<body>
	

    
    <header class="header">
	    
        <div class="branding">
            
            <div class="container position-relative">

				<nav class="navbar navbar-expand-lg" >
                    <h1 class="site-logo"><a class="navbar-brand" href="/"><img class="logo-icon" src="./assets/img/a5.png" alt="logo" width="150px"></a></h1>
				</nav>
	            
            </div><!--//container-->
            
        </div><!--//branding-->
        
        
    </header><!--//header-->
    
    
    <section class="hero-section">
	    <div class="container">
		    <div class="row figure-holder">
			    <div class="col-12 col-md-6 pt-3 pt-md-4">
				    <h2 class="site-headline font-weight-bold mt-lg-5 pt-lg-5">An app for your Big Days.</h2>
				    <div class="site-tagline mb-3">Come celebrate with us! </div>
				    <div class="cta-btns">
					    
					    <ul class="app-stores list-unstyled list-inline mx-auto mx-md-0 d-inline-block">
						    
						    <li class="list-inline-item"><a href="#"><img class="android" src="./landing-page/images/appstore-android.svg" alt="google play"></a></li>
					    </ul>
				    </div>
			    </div>
		    </div><!--//row-->
	    </div>
    </section><!--//hero-section-->
    <!-- Javascript -->          
    <script type="text/javascript" src="landing-page/plugins/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="landing-page/plugins/popper.min.js"></script>
    <script type="text/javascript" src="landing-page/plugins/bootstrap/js/bootstrap.min.js"></script>  
    
    <!-- Page Specific JS -->
    <script type="text/javascript" src="landing-page/plugins/jquery-flipster/dist/jquery.flipster.min.js"></script> 
    <script type="text/javascript" src="landing-page/js/flipster-custom.js"></script> 


</body>
</html> 

