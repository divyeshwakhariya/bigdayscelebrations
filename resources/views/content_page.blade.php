<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{URL::asset('assets/css/wedding.css')}}" rel="stylesheet" />
  <link href="{{URL::asset('assets/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <style>
	  @media only screen and (max-width: 768px)
	  {
		p {
			font-weight: 500;
			font-size: 16px!important;
			line-height: 30px;
			padding-left: 15px;
		}
		p > img {
			margin-left: -8px;
		}
		h2{
			font-size: 24px;
			font-weight:500;
			padding: 15px;
		}
	  }
  </style>
</head>

<body class="landing-page sidebar-collapse" style="background: #ffffff!important;">
	<div class="main main-raised">
		<div class="container">
			<div class="section">
				<div class="row">
					@include('header_logo')
				</div>
				@foreach($content as $Content)
				<div style="margin-top: 110px;">
					<h4 class="h4-02">{{$Content->Title}}</h4>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div>
		@foreach($content as $Content)
		<div class="mt-01">
			<img src="<?php echo url('./assets/images/articalsimage') ?>/{{$Content->Image}}"/>
		</div>
		@endforeach
	</div>
	@foreach($content as $Content)
	<div class="">
		<div>
			<!-- <img src="{!! html_entity_decode($Content->Message, ENT_QUOTES, 'UTF-8') !!}" class="img-resposive"/> -->
			<div class="img-resposive">{!! html_entity_decode($Content->Message, ENT_QUOTES, 'UTF-8') !!}</div>
		</div>
	</div><br>
	@endforeach

  <!--   Core JS Files   -->
  <script src="{{URL::asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/plugins/moment.min.js')}}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{URL::asset('assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{URL::asset('assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{URL::asset('assets/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>

</body>

</html>