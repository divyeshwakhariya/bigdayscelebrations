  <!-- Classic Modal -->
  <div class="container">
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Get Started</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
            @if($Details->DealUrl == "venues")
            <form action="{{route('venues-store')}}" method=post>
              {{csrf_field()}}
              <input type="hidden" value="{{$Details->DealUrl}}" name="RequestFor">
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                  <label class="bmd-label-floating">Full Name</label>
                  <input type="text" class="form-control" name="FullName" required>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group number_validation_parent">
                  <label class="bmd-label-floating">Mobile Number</label>
                  <input type="text" class="form-control number_validation_child no-leading-zero" name="MobileNo" autocomplete="off" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                  <label class="bmd-label-floating">Email Id</label>
                  <input type="email" class="form-control" name="EmailId" required>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                            <label class="bmd-label-floating">Function date</label>
                            <input type="Date" name="FunctionDate" class="form-control" required>
                        </div>
              </div>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group number_validation_parent">
                  <label class="bmd-label-floating">Number of guests(min 50)</label>
                  <input type="text" class="form-control number_validation_child" name="NoOfGuests" required>
                </div>
              </div>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group number_validation_parent">
                  <label class="bmd-label-floating">Number Of rooms(optional)</label>
                  <input type="text" class="form-control number_validation_child" name="NoOfRooms">
                </div>
              </div><br>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                  <label class="bmd-label-floating">Function Type</label>
                  <div class="row">
                    <div class="form-check ml-3">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="FunctionType" value="PreWedding">
                          Pre-Wedding
                        <span class="circle">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                    <div class="form-check ml-4">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="FunctionType" value="Wedding">
                          Wedding
                        <span class="circle">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div><br>
              <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                  <label class="bmd-label-floating">Function Time</label>
                  <div class="row">
                    <div class="form-check ml-3">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="FunctionTime" value="Evening">
                          Evening
                        <span class="circle">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                    <div class="form-check ml-4">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="FunctionTime" value="Day">
                          Day
                        <span class="circle">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-4 d-flex justify-content-center">
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-rose">Get Recommendations<div class="ripple-container"></div></button>
                </div>
              </div>
            </form>
            @else
            <!-- Venues======================================================================================= -->
            <form action="{{route('enquiryinsert')}}" method=post>
              {{csrf_field()}}
                <input type="hidden" value="{{$Details->DealUrl}}" name="RequestFor">
                <div class="col-lg-3 col-sm-4" style="padding: 0;">
                    <div class="form-group">
                        <label class="bmd-label-floating">Full Name</label>
                        <input type="text" name="FullName" class="form-control uppercase_letter char_validation" required>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-4" style="padding: 0;">
                    <div class="form-group number_validation_parent">
                        <label class="bmd-label-floating">Mobile Number</label>
                        <input type="text" name="MobileNo" class="form-control number_validation_child no-leading-zero" autocomplete="off" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required/>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-4" style="padding: 0;">
                    <div class="form-group">
                        <label class="bmd-label-floating">Email Id</label>
                        <input type="email" name="EmailId" class="form-control"  required>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-4" style="padding: 0;">
                    <div class="form-group">
                      <label class="bmd-label-floating">Date From</label>
                      <input type="date" name="Date" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-4" style="padding: 0;">
                    <div class="form-group">
                      <label class="bmd-label-floating">Details about my wedding</label>
                      <textarea type="text" name="Details" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Get Best Quote</button>
                </div>
            </form>
            @endif
          </div>
      </div>
    </div>
  </div>
</div>
  <!--  End Modal -->
