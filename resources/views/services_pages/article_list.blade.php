<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{URL::asset('assets/css/wedding.css')}}" rel="stylesheet" />
  <link href="{{URL::asset('assets/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  
</head>

<body class="landing-page sidebar-collapse" style="background: #ffffff!important;">
	<div class="main main-raised">
		<div class="container">
			<div class="section">
				<div class="row">
            @include('header_logo')
        </div>
			</div>
		</div>
	</div>
    @if($details)
    @foreach($details as $Details)
    <div class="container">
        <div class="d-flex mt-06">
            <a href="{{route('servicescontent',$Details->ServiceUrl)}}" style="width: 50%;" class="rounded img-fluid"><img src="{{URL::asset('assets/images/servicesimages')}}/{{$Details->Image}}"/></a>
            <p class="pl-3">
                <span class="heading-01">{{$Details->Title}}</span><br> 
                <span style="font-size: 17px;">{{$Details->Location}}</span><br><br>
                <span style="font-size: 17px;"><b>₹{{$Details->Price}}</b> per plate</span>
            </p>
        </div>
    </div>
    <div class="border-1 mt-3"></div>
    @endforeach
    @else
      <div style="position: absolute;bottom:186px">
        <img src="{{URL::asset('assets/img/no-data.jpg')}}"/>
      </div>
    @endif
  <!--   Core JS Files   -->
  <script src="{{URL::asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/plugins/moment.min.js')}}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{URL::asset('assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{URL::asset('assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{URL::asset('assets/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
</body>

</html>