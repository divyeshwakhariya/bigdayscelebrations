<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{URL::asset('assets/css/wedding.css')}}" rel="stylesheet" />
  <link href="{{URL::asset('assets/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css">
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- CSS Just for demo purpose, don't include it in your project -->
  
</head>

<body class="landing-page sidebar-collapse" style="background: #ffffff!important;">
	<div class="bg-07">
		<div class="container">
			<div class="row">
				@include('header_logo')
			</div>
		</div>
		<div class="container mt-9 overlay">
			<p>Leave it on our wedding planners</p>
			<h3>You focus on having fun, while our wedding planners take care of all arrangements</h3>
			<button type="button" class="btn-02" id="BestQuote" data-toggle="modal" data-target="#myModal" data-keyboard="false" data-backdrop="static">Get Best Quots<div class="ripple-container"></div></button>
		</div>
	</div>
	<div class="container count-venues">
		<div class="text-center font-weight-normal" style="font-size: 18px;">
			<div><b>50+</b> <span>Wedding Planners</span></div>
			<div class="mt-3"><b>100+</b> <span>Events Done</span></div>
			<div class="mt-3"><b>100+</b><span>Happy customers</span></div>
		</div>
	</div>
	@include('support')
	<div class="container mt-5 count-venues">
		<h4 class="text-center font-weight-bold" style="font-size: 25px;">Our portfolio</h4>
		<div class="line-bottom"></div>
		<!--<li class="dropdown nav-item text-center mt-5" style="list-style: none;">
            <a href="#" class="dropdown-toggle nav-link dropdown font-weight-bold" data-toggle="dropdown" style="color:#333;font-size:16px;">
               Banquet Halls
            </a>
            <div class="dropdown-menu dropdown-with-icons" style="padding: 0.3125rem 35px 0 0px;">
              <a href="" class="dropdown-item">
                 <b>Banquet Halls</b>
              </a>
              <a href="#" class="dropdown-item">
                <b>Spacious lawns</b>
              </a>
			  <a href="#" class="dropdown-item">
                <b>Destination wedding resorts</b>
              </a>
			  <a href="#" class="dropdown-item">
                <b>4/5 star hotels</b>
              </a>
            </div>
        </li>-->
        <div class="col-md-8 mr-auto ml-auto">
            <!-- Carousel Card -->
            <div class="card card-raised card-carousel">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
					<ol class="carousel-indicators">
					  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img class="d-block w-100" src="./assets/img/wedding-planner.jpg" alt="First slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="./assets/img/wedding-hotels.jpg" alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="./assets/img/wedding-resorts.jpg" alt="Third slide">
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					  <i class="material-icons">keyboard_arrow_left</i>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					  <i class="material-icons">keyboard_arrow_right</i>
					  <span class="sr-only">Next</span>
					</a>
				</div>
            </div>
		<!-- End Carousel Card -->
		</div>
	</div>
	<div class="container mt-5">
		<h4 class="text-center font-weight-bold" style="font-size: 25px;">1000+ Happy customers</h4>
		<div class="line-bottom"></div>
		<div class="col-md-8 mr-auto ml-auto">
            <!-- Carousel Card -->
            <div class="card card-raised card-carousel">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
					<ol class="carousel-indicators">
					  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="text-center" style="width: 20rem;">
								<h4 class="card-title">Rakhi & Hardik</h4>
								<a href="javascript:;" class="badge badge-success">5</a><span>&nbsp&nbsp Our dream come true</span>
								<p class="card-text p-review" style="color:#333">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
								Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
								when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							</div>
						</div>
						<div class="carousel-item">
							<div class="text-center" style="width: 20rem;">
								<h4 class="card-title">Rakhi & Hardik</h4>
								<a href="javascript:;" class="badge badge-success">5</a><span>&nbsp&nbsp Our dream come true</span>
								<p class="card-text p-review" style="color:#333">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
								Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
								when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					  <i class="material-icons">keyboard_arrow_left</i>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					  <i class="material-icons">keyboard_arrow_right</i>
					  <span class="sr-only">Next</span>
					</a>
				</div>
            </div>
		<!-- End Carousel Card -->
		</div>
	</div>
	<div class="container count-venues">
		<h4 class="text-center font-weight-bold" style="font-size: 25px;">How it works</h4>
		<div class="line-bottom"></div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">send</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Submit your request </p></div>
		</div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">phone_callback</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Share all your requirements with us</p></div>
		</div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">watch_later</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Get the best wedding planners in town</p></div>
		</div>
	</div>
	@include('services_pages/form_model')
  <!--   Core JS Files   -->
  <script src="{{URL::asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/plugins/moment.min.js')}}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{URL::asset('assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{URL::asset('assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{URL::asset('assets/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/change-event.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();
    });
  </script>
   <!-- Alert msg script -->
	<script>
		@if (session('alert'))
			swal("{{ session('alert') }}");
		@endif
	</script>
</body>

</html>