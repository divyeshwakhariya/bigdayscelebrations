<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
<meta charset = "UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="">
    <meta name="description"
        content="">
    <meta name="robots" content="noindex,nofollow">
    <title>Big Day Celeberation</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/libs/flot/css/float-chart.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('dist/css/style.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/extra-libs/multicheck/multicheck.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <script src="{{URL::asset('assets/editor/ckeditor.js')}}"></script>
	<script src="{{URL::asset('assets/editor/sample.js')}}"></script>
    <style>
        .bs{
		box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
	    }
        .validate{
            color:red;
        }
	.form-control-01 {
		display: block;
		padding: .375rem .75rem;
		font-size: 1rem;
		line-height: 1.5;
		background-color: #f8f9fa;
		background-clip: padding-box;
		border: 1px solid #cccccc82;
		border-radius: 4px;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
    .id_count_active{
		background: green;
		border-radius: 50px;
		padding: 4px;
		color: #fff;
	}
	.id_count_inactive{
		background: red;
		border-radius: 50px;
		padding: 4px;
		color: #fff;
	}
	.glow {
	  color: #fff;
	  text-align: center;
	  -webkit-animation: glow 1s ease-in-out infinite alternate;
	  -moz-animation: glow 1s ease-in-out infinite alternate;
	  animation: glow 1s ease-in-out infinite alternate;
	}

	@-webkit-keyframes glow {
	  from {
		text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px green, 0 0 40px green, 0 0 50px green, 0 0 60px green, 0 0 70px green;
	  }
	  
	  to {
		text-shadow: 0 0 20px #fff, 0 0 30px green, 0 0 40px green, 0 0 50px green, 0 0 60px green, 0 0 70px green, 0 0 80px green;
	  }
	}
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('admin/header') 
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         @include('admin/sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                <div class="col-lg-1" style="margin-top: -10px;">
                    <a style="font-size: 40px;color: #495057" href="{{route('services.index')}}"><i class="fas fa-arrow-alt-circle-left"></i></a>
                </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title mb-0">Servies Details</h5>
                                @foreach($servicesdetails as $details)
                                <form method=post id="services_details"  enctype="multipart/form-data" action="javascript:void(0)" >
								    {{csrf_field()}}
                                    @method('PATCH')
                                        <input type="hidden" id="ServicesId" value="{{$details->ServicesId}}">
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label>Title </label>
                                            <input type="text" value="{{$details->Title}}" class="form-control" id="title" name="Title" placeholder="Enter Title">
                                            <span id="error_title" class="validate alert-msg"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Image</label>
                                            <input type="file" class="form-control" id="image" name="Image"
                                                placeholder="Enter Image" accept=".png, .jpg, .jpeg" required>
                                            <span id="error_image" class="validate alert-msg"></span>
                                            <span style="color:red">Image Size 411*274</span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Services Url</label>
                                            <input type="text" value="{{$details->ServiceUrl}}" class="form-control char_validation_add"
                                                name="ServicesUrl" id="servicesurl" placeholder="Services Url" required>
                                            <span id="error_servicesurl" class="validate alert-msg"></span>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label>Price</label>
                                            <input type="text" value="{{$details->Price}}" class="form-control" id="price" name="Price" placeholder="Enter Price">
                                            <span id="error_price" class="validate alert-msg"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Location</label>
                                            <input type="text" value="{{$details->Location}}" class="form-control" id="location" name="Location" placeholder="Enter Location">
                                            <span id="error_location" class="validate alert-msg"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Address</label>
                                            <input type="text" value="{{$details->Address}}" class="form-control" id="address" name="Address" placeholder="Enter Address">
                                            <span id="error_address" class="validate alert-msg"></span>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label>Deal Url</label>
                                            <select name="DealUrl" id="dealurl" class="form-control" required>
                                                <option value="">Select</option>
                                                @foreach($Deals as $deals)
                                                <option value="{{$deals->Url}}" {{$details->DealUrl == $deals->Url ? 'selected': ''}}>{{$deals->Url}}</option>
                                                @endforeach
                                            </select>
                                            <span id="error_dealurl" class="validate alert-msg"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Status</label>
                                            <select name="Status" class="form-control" required>
                                                <option value="" disabled>Select</option>
                                                    <option value="" disabled>Select</option>
                                                    <option value="1" {{$details->Status == 1 ? 'selected': ''}}>Active</option>
                                                    <option value="2" {{$details->Status == 0 ? 'selected': ''}}>Inactive</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="<?php echo url('./assets/images/servicesimages') ?>/{{$details->Image}}" width="326px"/>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label>Description</label>
                                            <textarea title='System Description' id="editor" required>
                                                {!! html_entity_decode($details->Description, ENT_QUOTES, 'UTF-8') !!}
                                            </textarea>
                                            <span id="editor" class="validate alert-msg"></span>
                                        </div>
                                        <input type="hidden" id="editor1" value="" name="editor1">
                                    </div>
                                    <div class="card-body d-flex justify-content-center">
                                        <button type="button" class="btn btn-primary" id="submit">Submit</button>
                                    </div>
                                </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{URL::asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{URL::asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{URL::asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{URL::asset('dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{URL::asset('dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{URL::asset('dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->

    <!-- Form submit -->
    <script>
        $(document).ready(function(){
            var APP_URL = {!! json_encode(url('/')) !!};
                $("#submit").click(function() {
                    var error_msg = "";
                    //title
                    if($("#title").val()=="")
                        {	
                            $("#error_title").text("Please enter title");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_title").text("");
                        }
                    //image
                    if($("#image").val()=="")
                        {	
                            $("#error_image").text("Please select image");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_image").text("");
                        }
                    //url
                    if($("#servicesurl").val()=="")
                        {	
                            $("#error_servicesurl").text("Please enter url");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_servicesurl").text("");
                        }

                    //price
                    if($("#price").val()=="")
                        {	
                            $("#error_price").text("Please enter price");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_price").text("");
                        }
                    //location
                    if($("#location").val()=="")
                        {	
                            $("#error_location").text("Please enter location");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_location").text("");
                        }
                    //Address
                    if($("#address").val()=="")
                        {	
                            $("#error_address").text("Please enter address");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_address").text("");
                        }
                    //Deal Url
                    if($("#dealurl").find('option:selected').val() == "")
                        {	
                            $("#error_dealurl").text("Please select deal url");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#error_dealurl").text("");
                        }
                    if(error_msg)
                    {
                        return false;	
                    }
                    else{
                        
                        event.preventDefault();
                        ServicesId=$('#ServicesId').val();
                        var editor = CKEDITOR.instances['editor'].getData();
                        editor=$('#editor1').val(editor);
                        var form_data = new FormData(document.getElementById("services_details"));

                        $.ajax({
                            url:APP_URL+"/admin/services/"+ServicesId,
                            type:'post',
                            data:form_data,
                            dataType:'JSON',
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                console.log(data);
                                alert("Value updated successfully.");
                                window.location = '';
                            }
                        });
                    }
                });

                //check url exit or not
                $("#servicesurl").keyup(function() {
                Url=$("#servicesurl").val();
                $.ajax({
                    url:APP_URL+"/admin/exitservicesurl",
                    type:'get',
                    data:{Url:Url},
                    success: function (data) {
                        if(data == "Url Already Exist")
                        {
                            alert(data);
                            $('#servicesurl').val("");
                            $('#servicesurl').focus();
                        } 
                        else{

                        }
                    }
                });
            });
        });
    </script>
   
    <!-- Charts js Files -->
    <script src="{{URL::asset('assets/libs/flot/excanvas.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.time.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{URL::asset('dist/js/pages/chart/chart-page-init.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/change-event.js')}}"></script> 
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();
    </script>


    <!-- Text Editor -->
    <script>
        initSample();
    </script>

</body>

</html>