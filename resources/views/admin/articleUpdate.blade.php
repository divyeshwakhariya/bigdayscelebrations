<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords"
        content="">
    <meta name="description"
        content="">
    <meta name="robots" content="noindex,nofollow">
    <title>Big Day Celeberation</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/libs/flot/css/float-chart.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('dist/css/style.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/extra-libs/multicheck/multicheck.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <script src="{{URL::asset('assets/editor/ckeditor.js')}}"></script>
	<script src="{{URL::asset('assets/editor/sample.js')}}"></script>
    <style>
        .bs{
		box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
	    }
        .validate{
            color:red;
        }
	.form-control-01 {
		display: block;
		padding: .375rem .75rem;
		font-size: 1rem;
		line-height: 1.5;
		background-color: #f8f9fa;
		background-clip: padding-box;
		border: 1px solid #cccccc82;
		border-radius: 4px;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
    .id_count_active{
		background: green;
		border-radius: 50px;
		padding: 4px;
		color: #fff;
	}
	.id_count_inactive{
		background: red;
		border-radius: 50px;
		padding: 4px;
		color: #fff;
	}
	.glow {
	  color: #fff;
	  text-align: center;
	  -webkit-animation: glow 1s ease-in-out infinite alternate;
	  -moz-animation: glow 1s ease-in-out infinite alternate;
	  animation: glow 1s ease-in-out infinite alternate;
	}

	@-webkit-keyframes glow {
	  from {
		text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px green, 0 0 40px green, 0 0 50px green, 0 0 60px green, 0 0 70px green;
	  }
	  
	  to {
		text-shadow: 0 0 20px #fff, 0 0 30px green, 0 0 40px green, 0 0 50px green, 0 0 60px green, 0 0 70px green, 0 0 80px green;
	  }
	}
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('admin/header') 
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         @include('admin/sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title mb-0">Form Elements</h5>
                                @foreach($article as $articles)
                                <form method="post" id="articals_details"  enctype="multipart/form-data" action="javascript:void(0)" >
                                        {{csrf_field()}}
                                        @method('PATCH')
                                        <input type="hidden" id="ArticleId" value="{{$articles->ArticleId}}">
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <label>Title </label>
                                            <input type="text" class="form-control" id="title" name="Title"
                                                placeholder="Enter Title" value="{{$articles->Title}}">
                                                <span id="title_mobile" class="validate alert-msg"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Image</label>
                                            <input type="file" value="{{$articles->Image}}" id="image" class="form-control" name="Image"
                                                placeholder="Enter Image" accept=".png, .jpg, .jpeg">
                                                <span id="image_mobile" class="validate alert-msg"></span>
                                            <span style="color:red">Image Size 411*274</span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Url</label>
                                            <input type="text" value="{{$articles->Url}}" class="form-control char_validation_add"
                                                name="Url" id="url" placeholder="Enter Url">
                                                <span id="url_mobile" class="validate alert-msg"></span>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-4">
                                            <img src="<?php echo url('./assets/images/articalsimage') ?>/{{$articles->Image}}" width="326px"/>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Status </label>
                                            <select name="Status" class="form-control">
                                                <option value="" disabled>Select</option>
                                                <option value="1" {{$articles->Status == 1 ? 'selected': ''}}>Active</option>
                                                <option value="2" {{$articles->Status == 0 ? 'selected': ''}}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <label>Text Editor</label>
                                            <textarea title='System Description' id="editor">{!! html_entity_decode($articles->Message, ENT_QUOTES, 'UTF-8') !!}</textarea>
                                        </div>
                                        <input type="hidden" id="editor1" value="" name="editor1">
                                    </div>
                                    <div class="card-body d-flex justify-content-center">
                                        <button type="button" class="btn btn-primary" id="submit">Submit</button>
                                    </div>
                                </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{URL::asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{URL::asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{URL::asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{URL::asset('dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{URL::asset('dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{URL::asset('dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->

    <!-- Form submit -->
    <script>
        $(document).ready(function(){
            var APP_URL = {!! json_encode(url('/')) !!};
                $("#submit").click(function() {

                    var error_msg = "";
                    //title
                    if($("#title").val()=="")
                        {	
                            $("#title_mobile").text("Please enter title");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#title_mobile").text("");
                        }
                    //image
                    if($("#image").val()=="")
                        {	
                            $("#image_mobile").text("Please select image");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#image_mobile").text("");
                        }
                    //url
                    if($("#url").val()=="")
                        {	
                            $("#url_mobile").text("Please enter url");
                            error_msg = "Error";
                        }
                    else
                        {
                            $("#url_mobile").text("");
                        }
                    if(error_msg)
                    {
                        return false;	
                    }
                    else{
                        event.preventDefault();
                        ArticleId=$('#ArticleId').val();
                        var editor = CKEDITOR.instances['editor'].getData();
                        editor=$('#editor1').val(editor);
                        var form_data = new FormData(document.getElementById("articals_details"));

                        $.ajax({
                            url:APP_URL+"/admin/article/"+ArticleId,
                            type:'post',
                            data:form_data,
                            dataType:'JSON',
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                console.log(data);
                                alert("Value added successfully.");
                                //window.location = 'article';
                            }
                        });
                    }
                });
        });
    </script>
   
    <!-- Charts js Files -->
    <script src="{{URL::asset('assets/libs/flot/excanvas.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.time.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
    <script src="{{URL::asset('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{URL::asset('dist/js/pages/chart/chart-page-init.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
    <script src="{{URL::asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/change-event.js')}}"></script> 
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();
    </script>


    <!-- Text Editor -->
    <script>
        initSample();
    </script>

</body>

</html>