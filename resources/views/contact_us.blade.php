<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{URL::asset('assets/css/wedding.css')}}" rel="stylesheet" />
  <link href="{{URL::asset('assets/css/material-kit.css?v=2.0.7')}}" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css">
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- CSS Just for demo purpose, don't include it in your project -->
  
</head>

<body class="landing-page sidebar-collapse" style="background: #ffffff!important;">
	<div class="">
		<div class="container">
			<div class="row">
                @include('header_logo')
				
			</div>
		</div>
        <div class="card card-raised card-carousel">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="3000">
					<ol class="carousel-indicators">
					  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img class="d-block w-100" src="./assets/img/contact-us-woman.jpg" alt="First slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="./assets/img/ContactUs-02.jpg" alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="./assets/img/ContactUs-01.jpg" alt="Third slide">
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					  <i class="material-icons">keyboard_arrow_left</i>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					  <i class="material-icons">keyboard_arrow_right</i>
					  <span class="sr-only">Next</span>
					</a>
				</div>
        </div>
	</div>
    <div class="container mt-5">
        <form action="{{route('contactus')}}" method=post>
        {{csrf_field()}}
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                    <label class="bmd-label-floating">Looking For</label>
                    <select name="LookingFor" class="form-control" required>
                        <option disabled value="">Select An Option</option>
                        <option value="1">Complete Wedding Planning</option>
                        <option value="2">Partial Wedding Planning</option>
                        <option value="3">Vender Management</option>
                        <option value="4">Best Management</option>
                        <option value="5">Venue Booking Only</option>
                        <option value="6">Wedding Only</option>
                        <option value="7">Wedding Packages</option>
                        <option value="8">Other Requirements</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                    <label class="bmd-label-floating">Full Name</label>
                    <input type="text" name="FullName" class="form-control uppercase_letter char_validation" required>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group number_validation_parent">
                    <label class="bmd-label-floating">Contact Number</label>
                    <input type="text" name="ContactNo" class="form-control number_validation_child no-leading-zero" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" required>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                    <label class="bmd-label-floating">Email Id</label>
                    <input type="email" name="EmailId" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                    <label class="bmd-label-floating">Your Location</label>
                    <input type="text" name="Location" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group number_validation_parent">
                    <label class="bmd-label-floating">Prefered Wedding Location or Venue</label>
                    <input type="text" name="WeddignLocation" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group">
                    <label class="bmd-label-floating">Date From</label>
                    <input type="date" name="FromDate" class="form-control" required>
                </div>
                <div class="form-group">
                    <label class="bmd-label-floating">Date To</label>
                    <input type="date" name="ToDate" class="form-control" required>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4" style="padding: 0;">
                <div class="form-group number_validation_parent">
                    <label class="bmd-label-floating">Mention Details Requirments</label>
                    <textarea type="text" name="Requirements" class="form-control" required></textarea>
                </div>
            </div><br>
            <div class="form-group d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">Send Details</button>
            </div>
        </form>
    </div>
  <!--   Core JS Files   -->
  <script src="{{URL::asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/plugins/moment.min.js')}}"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="{{URL::asset('assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{URL::asset('assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="{{URL::asset('assets/js/material-kit.js?v=2.0.7')}}" type="text/javascript"></script>
  <script src="{{URL::asset('assets/js/change-event.js')}}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //init DateTimePickers
      materialKit.initFormExtendedDatetimepickers();
    });
  </script>
    <!-- Alert msg script -->
    <script>
    @if (session('alert'))
        swal("{{ session('alert') }}");
    @endif
    </script>
</body>

</html>