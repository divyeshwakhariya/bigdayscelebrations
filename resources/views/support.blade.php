<div class="container">
		<h4 class="text-center font-weight-bold" style="font-size: 25px;">We know what you need</h4>
		<div class="line-bottom"></div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">thumb_up</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Best quality service</p></div>
			<div><p class="font-weight-bold " style="font-size: 16px;padding: 0 40px 0 50px;color: #6c757d;">We believe in delivering more than expected</p></div>
		</div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">payments</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Affordable prices</p></div>
			<div><p class="font-weight-bold " style="font-size: 16px;padding: 0 40px 0 50px;color: #6c757d;">We price our services to beat the budget blues</p></div>
		</div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">content_copy</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Compare styles</p></div>
			<div><p class="font-weight-bold " style="font-size: 16px;padding: 0 40px 0 50px;color: #6c757d;">We understand it is very important for you to compare styles</p></div>
		</div>
		<div class="mt-5 d-flex justify-content-center">
			<span class="material-icons icon round">support_agent</span>
		</div>
		<div class="mt-4 text-center">
			<div><p class="font-weight-bold" style="font-size: 18px;">Customer service</p></div>
			<div><p class="font-weight-bold " style="font-size: 16px;padding: 0 40px 0 50px;color: #6c757d;">We are happy to assist you through call, text or Whatsapp</p></div>
		</div>
	</div>