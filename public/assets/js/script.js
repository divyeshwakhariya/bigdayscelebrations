// https://builtbymax.de
// Maximilian Kobus | KØBY

(function () {
  document.addEventListener('DOMContentLoaded', function () {

    'use strict';

    Slider.init();
    conf.InfoBox();

  });
})();

const Slider = {

  init: () => {
    Slider.initialize();
  },

  //  Click Handler Button
  // - - - - - - - - - - - - - - - - - - - - - - - - - -

  initialize: () => {
    var mySwiper = new Swiper('.swiper-container', {
      direction: 'horizontal',
      loop: false,
      grabCursor: true,
      slidesPerView: 1.35,
      centeredSlides: true,
      spaceBetween: 20,
      roundLengths: true,
      pagination: {
        el: '.swiper-pagination' } });



    mySwiper.on('slideChange', function () {
      if (mySwiper.activeIndex === 1) {
        let mobileContainer = conf.qS('.mobile-container');
        mobileContainer.className = '';
        mobileContainer.classList.add('mobile-container');
        mobileContainer.classList.add('image-2');
      } else if (mySwiper.activeIndex === 2) {
        let mobileContainer = conf.qS('.mobile-container');
        mobileContainer.className = '';
        mobileContainer.classList.add('mobile-container');
        mobileContainer.classList.add('image-3');
      } else if (mySwiper.activeIndex === 0) {
        let mobileContainer = conf.qS('.mobile-container');
        mobileContainer.className = '';
        mobileContainer.classList.add('mobile-container');
        mobileContainer.classList.add('image-1');
      }
    });
  } };


//  Config Functions
// - - - - - - - - - - - - - - - - - - - - - - - - - -

const conf = {
  qS: selector => {
    return document.querySelector(selector);
  },
  qSA: selector => {
    return document.querySelectorAll(selector);
  },
  CqS: (container, selector) => {
    return container.querySelector(selector);
  },
  InfoBox: () => {
    let toggle = conf.qS('.infobox-container .infobox-toggle'),
    detail = conf.qS('.infobox-container .infobox-detail-container');

    if (toggle) {
      toggle.onclick = e => {
        e.preventDefault();
        detail.classList.toggle('active');
      };
    }
  } };