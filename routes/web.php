<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('privacy-policy', function () {
    return view('privacy_policy');
});
Route::post('enquiryinsert','WeddingController@store')->name('enquiryinsert');
Route::post('contactus','WeddingController@contactUsStore')->name('contactus');
Route::post('venues-store','WeddingController@venueStore')->name('venues-store');
Route::get('wedding','WeddingController@index');
Route::get('services','WeddingController@services');
Route::get('contactus','WeddingController@contactUs');
Route::get('venues','WeddingController@venues')->name('venues');
Route::get('photographer','WeddingController@photographer')->name('photographer');
Route::get('makeup','WeddingController@makeup')->name('makeup');
Route::get('mehendi','WeddingController@mehendi')->name('mehendi');
Route::get('bandbaja','WeddingController@bandBaja')->name('bandbaja');
Route::get('decoration','WeddingController@decoration')->name('decoration');
Route::get('transport','WeddingController@transport')->name('transport');
Route::get('weddingplanning','WeddingController@weddingPlanning')->name('weddingplanning');
Route::get('dj','WeddingController@dj')->name('dj');
Route::get('caterers','WeddingController@caterers')->name('caterers');
Route::get('details/{slug}','WeddingController@content')->name('details');
Route::get('content','WeddingController@content')->name('content');
Route::get('dealslist/{slug}','WeddingController@dealsList')->name('dealslist');
Route::get('servicescontent/{slug}','WeddingController@servicesContent')->name('servicescontent');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::resource('index','admin\IndexController');
// Route::resource('article','admin\ArticleController');
// Route::get('exitUrl','admin\ArticleController@exitUrl');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {
    Route::resource('index','Admin\IndexController');
    Route::resource('article','Admin\ArticleController');
    Route::get('exitUrl','Admin\ArticleController@exitUrl');
    Route::get('articlestatus','Admin\ArticleController@Status');

    //images
    Route::get('images','Admin\ImageController@index');
    Route::post('images','Admin\ImageController@store')->name('images.store');
    Route::post('image-library','Admin\ImageController@store');

    //services details
    Route::resource('services','Admin\ServiceDetailsController');
    Route::get('exitservicesurl','Admin\ServiceDetailsController@exitUrl');
    Route::get('status','Admin\ServiceDetailsController@Status');
    //Deals
    Route::resource('deals','Admin\DealsController');
    Route::get('dealsUrl','Admin\DealsController@exitUrl');
    Route::get('dealsstatus','Admin\DealsController@Status');

    //Enquiries
    Route::get('enquiries','Admin\EnquiriesController@enquiries');
    Route::post('enquiries','Admin\EnquiriesController@getEnquiries')->name('getEnquiries');
});