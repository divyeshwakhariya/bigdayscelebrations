<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = ['LookingFor','FullName','ContactNo','EmailId','Location','WeddignLocation','FromDate',
                            'ToDate','Requirements','CreatedDate'];
    public $timestamps=false;
	protected $table = 'contact_us';
	protected $primaryKey = 'Id';
}
