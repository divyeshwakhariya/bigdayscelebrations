<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

class ImageAlbum extends Model
{
    protected $fillable = ['ImageName','ServiceId','CreatedDate'];
    public $timestamps = false;
	protected $table="image_albums";
	protected $primaryKey="Id";
}
