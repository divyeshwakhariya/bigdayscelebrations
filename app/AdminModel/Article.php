<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['Title','Image','Url','Message','CreatedDate','LastUpdatedDate','Status'];
    public $timestamps = false;
	protected $table="article";
	protected $primaryKey="ArticleId";
}
