<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

class ServiceDetails extends Model
{
    protected $fillable = ['Title','DealUrl','Image','Price','Location','Address','Description',
                            'Status','ServiceUrl','CreatedDate','LastUpdatedDate'];
    public $timestamps = false;
	protected $table="services_details";
	protected $primaryKey="ServicesId";
}
