<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;

class Deals extends Model
{
    protected $fillable = ['Deal','Status','Image','Url','CreatedDate','LastUpdatedDate'];
    public $timestamps = false;
	protected $table="deals";
	protected $primaryKey="DealsId";
}
