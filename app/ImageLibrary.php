<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageLibrary extends Model
{
    protected $table = 'image_library';
}
