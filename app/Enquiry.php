<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable = ['FullName','MobileNo','EmailId','Date','RequestFor','DetailsWedding','CreatedDate'];
    public $timestamps=false;
	protected $table = 'enquiry';
	protected $primaryKey = 'Id';
}
