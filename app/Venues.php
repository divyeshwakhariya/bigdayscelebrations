<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venues extends Model
{
    protected $fillable = ['FullName','MobileNo','EmailId','FunctionDate','NoOfGuests',
    'NoOfRooms','FunctionType','FunctionTime','RequestFor','CreatedDate',];
    public $timestamps=false;
	protected $table = 'venues';
	protected $primaryKey = 'Id';
}
