<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiry;
use App\ContactUs;
use App\Venues;
use DateTime;
use DB;
use App\AdminModel\Article;
use App\AdminModel\Deals;
use App\AdminModel\ServiceDetails;

class WeddingController extends Controller
{
    public function index(){

        $article=DB::select('SELECT * FROM article where Status="1" ORDER BY ArticleId DESC ');
        return view('wedding')->with('article',$article);
    }

    //services
    public function services(){

        $Deals=DB::select('SELECT * FROM deals where Status="1"');
        return view('services')->with('Deals',$Deals);
    }

    //contact up
    public function contactUs(){
        return view('contact_us');
    }

    //Venues
    public function venues(){
        
        return view('services_pages/venues');
    }

    //photographer
    public function photographer(){
        return view('services_pages/photographer');
    }

    //photographer
    public function makeup(){
        return view('services_pages/makeup');
    }

    //mehndi
    public function mehendi(){
        return view('services_pages/mehendi');
    }

    //bandBaja
    public function bandBaja(){
        return view('services_pages/band_baja');
    }

    //decoration
    public function decoration(){
        return view('services_pages/decoration');
    }

    //transport
    public function transport(){
        return view('services_pages/transport');
    }

    //transport
    public function weddingPlanning(){
        return view('services_pages/wedding_planning');
    }

    //dj
     public function dj(){
        return view('services_pages/dj');
    }

    //caterers
     public function caterers(){
        return view('services_pages/caterers');
    }

    //servicesContent
    public function servicesContent($Url){
        $services_details=DB::select('SELECT ServicesId,Title,Image,DealUrl,ServiceUrl,Price,
                                    Location,Address,Description 
                                    FROM services_details where Status="1" AND ServiceUrl=?',[$Url]);
        $sercicesid=$services_details[0]->ServicesId;
        $image_albums=DB::select('SELECT * FROM image_albums where ServiceId=?',[$sercicesid]);
        return view('services_pages/article_details')->with('details',$services_details)
                    ->with('image_albums',$image_albums);
    }

    //content
    public function content($Url){
        $content=DB::select('SELECT * FROM article where Url=?',[$Url]);
        return view('content_page')->with('content',$content);
    }

    //articleList
    public function dealsList($Url){
        $services_details=DB::select('SELECT * FROM services_details where Status="1" AND DealUrl=?',[$Url]);
        return view('services_pages/article_list')->with('details',$services_details);
    }

    public function store(Request $request)
    {
        //return $request->all();die;
        $CreatedDate= new DateTime();
        $Enquiry = Enquiry::create(['FullName'=>$request->FullName,'MobileNo'=>$request->MobileNo,
        'EmailId'=>$request->EmailId,'Date'=>$request->Date,'RequestFor'=>$request->RequestFor,'DetailsWedding'=>$request->Details,
        'CreatedDate'=>$CreatedDate]);
        return redirect()->back() ->with('alert', 'Your request is with us, will get you in  touch with you soon.');
    }

    public function contactUsStore(Request $request)
    {
        //return $request->all();die;
        $CreatedDate= new DateTime();
        $ContactUs = ContactUs::create(['LookingFor'=>$request->LookingFor,'FullName'=>$request->FullName,'ContactNo'=>$request->ContactNo,
        'EmailId'=>$request->EmailId,'Location'=>$request->Location,'WeddignLocation'=>$request->WeddignLocation,
        'FromDate'=>$request->FromDate,'ToDate'=>$request->ToDate,'Requirements'=>$request->Requirements,'CreatedDate'=>$CreatedDate]);
        return redirect()->back() ->with('alert', 'Your request is with us, will get you in touch with you soon.');
    }

    public function venueStore(Request $request)
    {
        $CreatedDate   = new DateTime();
        //return $request->all();die;
        $ContactUs = Venues::create(['FullName'=>$request->FullName,'MobileNo'=>$request->MobileNo,'EmailId'=>$request->EmailId,
        'FunctionDate'=>$request->FunctionDate,'NoOfGuests'=>$request->NoOfGuests,'NoOfRooms'=>$request->NoOfRooms,
        'FunctionType'=>$request->FunctionType,'FunctionTime'=>$request->FunctionTime,'RequestFor'=>$request->RequestFor,'CreatedDate'=>$CreatedDate]);
        return redirect()->back() ->with('alert', 'Your request is with us, will get you in  touch with you soon.');
    }
}
