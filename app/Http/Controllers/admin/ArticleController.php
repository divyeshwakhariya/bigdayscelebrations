<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Article;
use DateTime;
use DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article=DB::select('SELECT * FROM article ORDER BY ArticleId DESC');
        return view('admin/article')->with('article',$article);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Url = str_replace(' ', '-', $request->Url);

        $CreatedDate= new DateTime();
        $file = $request->file('Image');
            if($file){
                $image = $request->file('Image');
                $Image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/images/articalsimage');
                $image->move($destinationPath, $Image);

                $data = array('Title'=>$request->Title,'Status'=>$request->Status,'Image'=>$Image,'Url'=>$Url,'Message'=>$request->editor1,
                'CreatedDate'=>$CreatedDate);

                $Article = Article::create($data);
                return $Article;
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Message=DB::select('SELECT * FROM article where ArticleId=?',[$id]);
        return view('admin/message-view')->with('Message',$Message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article=DB::select('SELECT * FROM article where ArticleId=?',[$id]);
        return view('admin/articleUpdate')->with('article',$article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Url = str_replace(' ', '-', $request->Url);
        $ArticleId=$id;
        $CreatedDate= new DateTime();
        $file = $request->file('Image');
            if($file){
                $image = $request->file('Image');
                $Image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/images/articalsimage');
                $image->move($destinationPath, $Image);

                $data = array('Title'=>$request->Title,'Status'=>$request->Status,'Image'=>$Image,'Url'=>$Url,'Message'=>$request->editor1,
                'CreatedDate'=>$CreatedDate);
                $Article = Article::where('ArticleId',$ArticleId)->update($data);
                return $Article;
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
       
    }

    public function exitUrl(Request $request)
    {
        $Url= $request->Url;

        $Url=DB::select("SELECT Url FROM article where Url=?",[$Url]);
    	//$url=$Url[0]->Url;
    	if(isset($Url[0]->Url))
    	{
    		echo "Url Already Exist";
    	}
        else{
            return 0;
        }
    }

    //Status Update
    public function Status(Request $request)
    {
        $Id=$request->Id;
        $data=array('Status'=>$request->Status);
        Article::where('ArticleId',$Id)->update($data);
    }
}
