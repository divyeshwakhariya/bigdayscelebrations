<?php

namespace App\Http\Controllers\admin;

use App\AdminModel\Deals;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\ServiceDetails;
use App\AdminModel\ImageAlbum;
use DateTime;
use DB;
use File;

class ServiceDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Deals=Deals::all();
        $ServiceDetails=ServiceDetails::all();
        return view('admin/services-details')
                ->with('Deals',$Deals)
                ->with('ServiceDetails',$ServiceDetails);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Title=strtolower($request->Title);
        $Title = str_replace(' ', '-', $Title);
        $CreatedDate= new DateTime();


        //Store Multipel images
        $images=array();
        if($files=$request->file('Album')){
            foreach($files as $file){
                $name=$file->getClientOriginalName();
                $destinationPath = public_path('assets/images/Album');
                $file->move($destinationPath,$name);
                $images[]=$name;
            }
        }
      
        $file = $request->file('Image');
            if($file){
                $image = $request->file('Image');
                $Image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/images/servicesimages');
                $image->move($destinationPath, $Image);

                $data = array('Title'=>$request->Title,'DealUrl'=>$request->DealUrl,
                            'ServiceUrl'=>$Title ,'Image'=>$Image,'Description'=>$request->editor1,
                            'Price'=>$request->Price,'Location'=>$request->Location,'Address'=>$request->Address,
                            'Status'=>$request->Status,'CreatedDate'=>$CreatedDate);

                $ServiceDetails = ServiceDetails::create($data);
               $ServicesId= $ServiceDetails->ServicesId;

               for($i=0;$i<count($images);$i++)
               {
                   ImageAlbum::insert( [
                       'ImageName'=>  $images[$i],
                       'ServiceId'=> $ServicesId,
                       'CreatedDate'=>new DateTime()
                   ]);
               }

                return $ServiceDetails;
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Message=DB::select('SELECT * FROM services_details where ServicesId=?',[$id]);
        return view('admin/services-view')->with('Message',$Message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicesdetails=DB::select('SELECT * FROM services_details where ServicesId=?',[$id]);
        $Deals=Deals::all();
        return view('admin/services-update')
                    ->with('servicesdetails',$servicesdetails)
                    ->with('Deals',$Deals);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ServicesUrl = str_replace(' ', '-', $request->ServicesUrl);
        $CreatedDate= new DateTime();
        $ServicesId=$id;
        $file = $request->file('Image');
            if($file){
                $image = $request->file('Image');
                $Image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/images/servicesimages');
                $image->move($destinationPath, $Image);

                $data = array('Title'=>$request->Title,'DealUrl'=>$request->DealUrl,
                            'ServiceUrl'=>$ServicesUrl,'Image'=>$Image,'Description'=>$request->editor1,
                            'Price'=>$request->Price,'Location'=>$request->Location,'Address'=>$request->Address,
                            'Status'=>$request->Status,'CreatedDate'=>$CreatedDate);

                $ServiceDetails = ServiceDetails::where('ServicesId',$ServicesId)->update($data);
                
                return $ServiceDetails;
            }
    }


    public function exitUrl(Request $request)
    {
        $Url= $request->Url;

        $Url=DB::select("SELECT ServiceUrl FROM services_details where ServiceUrl=?",[$Url]);
    	//$url=$Url[0]->Url;
    	if(isset($Url[0]->ServiceUrl))
    	{
    		echo "Url Already Exist";
    	}
        else{
            return 0;
        }
    }

    //Status Update
    public function Status(Request $request)
    {
        $Id=$request->Id;
        $data=array('Status'=>$request->Status);
        ServiceDetails::where('ServicesId',$Id)->update($data);
    }
}
