<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class EnquiriesController extends Controller
{
    public function enquiries()
    {
        return view('admin/enquiries');
    }

    public function getEnquiries(Request $request)
    {
        $enquiryFor = $request->enquiryFor;
        $fromDate = $request->fromdate;
        $toDate = $request->todate;
        $Date = date('Y-m-d');


        if($enquiryFor=='Venues')
        {
            $fetch = DB::select("SELECT
            ven.FullName,
            ven.MobileNo,
            ven.EmailId,
            ven.FunctionDate,
            ven.NoOfGuests,
            ven.NoOfRooms,
            ven.FunctionType,
            ven.FunctionTime,
            ven.CreatedDate EnquiryDate,
            ven.RequestFor,
            ser.Title as ServiceProvider
            FROM
            venues AS ven LEFT JOIN services_details AS ser ON ven.Provider = ser.ServicesId
            WHERE DATE( ven.CreatedDate ) BETWEEN '$fromDate' AND '$toDate' ORDER BY ven.Id DESC");
        }
        elseif($enquiryFor =='ContactUs')
        {
            $fetch = DB::select("SELECT
                con.LookingFor,
                con.FullName,
                con.ContactNo,
                con.EmailId,
                con.Location,
                con.WeddignLocation,
                con.FromDate,
                con.ToDate,
                con.Requirements,
                con.CreatedDate 
            FROM
                contact_us AS con 
            WHERE
                DATE( con.CreatedDate ) BETWEEN '$fromDate' AND '$toDate' ORDER BY con.Id DESC");
        }
        else{
            $fetch = DB::select("SELECT
                eq.FullName,
                eq.MobileNo,
                eq.Date,
                eq.EmailId,
                eq.DetailsWedding,
                eq.RequestFor,
                eq.CreatedDate,
                eq.Provider,
                sd.Title 
            FROM
                enquiry AS eq LEFT JOIN services_details AS sd ON eq.Provider = sd.ServicesId 
            WHERE DATE( eq.CreatedDate ) BETWEEN '$fromDate' AND '$toDate'");
        }

        return view('admin.enquiries')->with('FetchData',$fetch)
        ->with('FromDate',$fromDate)->with('ToDate',$toDate)->with('enquiryFor',$enquiryFor);
    }
}
