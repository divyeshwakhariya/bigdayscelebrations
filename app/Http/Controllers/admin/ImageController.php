<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ImageLibrary;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function index()
    {
        $Images = ImageLibrary::all()->sortByDesc("id");
        
        return view('admin.images')->with('Images',$Images);
    }

    public function store(request $request) 
    {
        $this->validate($request, [
            'filenames' => 'required',
            'filenames.*' => 'mimes:jpg,jpeg,png'
        ]);

        
        if($request->hasfile('filenames'))
        {
            foreach($request->file('filenames') as $file)
            {
                $rand = rand(11111,99999);
                $name = time().$rand.'.'.$file->extension();
                $file->move(public_path().'/assets/images/imagelibrary/', $name);  
                $Lib= new ImageLibrary();
                $Lib->filenames=$name;
                $Lib->save();
            }
        }

        return back()->with('success', 'Data Your files has been successfully added');
    }

    public function library()
    {
        
        return view('admin.imagelibrary');
    }
}
