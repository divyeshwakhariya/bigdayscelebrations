<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AdminModel\Deals;
use DateTime;
use DB;

class DealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Deal=Deals::all();
        return view('admin/deals')->with('Deal',$Deal);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Url = str_replace(' ', '-', $request->Url);

        $CreatedDate= new DateTime();
        $file = $request->file('Image');
            if($file){
                $image = $request->file('Image');
                $Image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/images/Dealsimage');
                $image->move($destinationPath, $Image);

                $data = array('Deal'=>$request->Title,'Status'=>$request->Status,'Image'=>$Image,'Url'=>$Url,'CreatedDate'=>$CreatedDate);
                $Deals = Deals::create($data);
                return redirect()->back() ->with('alert', 'Value Inserted Successfully..!');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deals=DB::select('SELECT * FROM deals where DealsId=?',[$id]);
        return view('admin/deals-update')
                    ->with('deals',$deals);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Url = str_replace(' ', '-', $request->Url);

        $CreatedDate= new DateTime();
        $file = $request->file('Image');
            if($file){
                $image = $request->file('Image');
                $Image = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('assets/images/Dealsimage');
                $image->move($destinationPath, $Image);

                $data = array('Deal'=>$request->Title,'Image'=>$Image,'Url'=>$Url,'CreatedDate'=>$CreatedDate);
                $Deals = Deals::where('DealsId',$id)->update($data);
                return redirect()->back() ->with('alert', 'Value updated Successfully..!');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function exitUrl(Request $request)
    {
        $Url= $request->Url;

        $Url=DB::select("SELECT Url FROM deals where Url=?",[$Url]);
    	//$url=$Url[0]->Url;
    	if(isset($Url[0]->Url))
    	{
    		echo "Url Already Exist";
    	}
        else{
            return 0;
        }
    }

    //Status Update
    public function Status(Request $request)
    {
        $Id=$request->Id;
        $data=array('Status'=>$request->Status);
        Deals::where('DealsId',$Id)->update($data);
    }
}
