<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ArticleSaveController extends Controller
{
    public function articleSave()
    {
        $pagelist=array();
        $article=DB::select('SELECT ArticleId,Title,Image,Url FROM article ORDER BY ArticleId DESC');
        
        foreach($article as $art){
            $ArticleId=$art->ArticleId;
            $Title=$art->Title;
            $Image=$art->Image;
            $Url=$art->Url;
            $destinationPath =('assets/images/articalsimage');
            //concatenate base url and image path
            $Image=url('/')."/$destinationPath/$Image";
            //concatenate base url and page Url
            $Url=url('/')."/details/$Url";
            // //create associative array
            $pageDetails=array('articleId'=>$ArticleId,'pageHeader'=>$Title,'pageImage'=>$Image,'pageLink'=>$Url);
            $pagelist[]=$pageDetails;

        }
        if($pagelist){
            
            return response()->json(["statusCode"=>"200","message"=>"Success","PageList"=>$pagelist,]); 
        }
        else{
            return response()->json(["statusCode"=>"400","message"=>"Error"]);
        }
        return response()->json($pagelist);
    }
}
